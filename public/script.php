<?php
header('Content-Type: text/html; charset=UTF-8');

if($_POST['send'] == 'Отправить'){
    if ($_POST['checkbox'] == ''){
        print ('Вы должны быть ознакомлены с контрактом');
       exit();
    }
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        print('Поле ФИО не заполнено<br/>');
        $errors = TRUE;
    }
    else if(!preg_match("/^[a-zA-Zа-яёА-ЯЁ]{3,70}/", $_POST['fio'])){
        print('ФИО введены неверно.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['email'])) {
        print('Поле E-mail не заполнено<br/>');
        $errors = TRUE;
    }
    else if(!preg_match("/^\w+([\.\w]+)*\w@\w((\.\w)*\w+)*\.\w{2,3}$/", $_POST['email'])){
        print('E-mail введён неверно<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['birth_date'])) {
        print('Дата рождения не заполнена<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['gender'])) {
        print('Пол не указан<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['limbs'])) {
        print('Не указано количство конечностей<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['superpowers'])) {
        print('Не указаны сверхспособности<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['biographia'])) {
        print('Не заполнена биография<br/>');
        $errors = TRUE;
    }
    if ($errors) exit;

    // Сохранение в базу данных.
    $user = 'u20398';
    $pass = '7592324';
    $db = new PDO('mysql:host=localhost;dbname=u20398', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    $fio = $_POST['fio'];
    $email = $_POST['email'];
    $birth_date = $_POST['birth_date'];
    $gender = $_POST['gender'];
    $limbs = $_POST['limbs'];
    $superpowers = $_POST['superpowers'];
    $biographia = $_POST['biographia'];

    try {
      $stmt = $db->prepare("INSERT INTO application (fio, email, birth_date, gender, limbs, superpowers, biographia) VALUES (:fio, :email, :birth_date, :gender, :limbs, :superpowers, :biographia)");
      $stmt -> execute(array('fio'=>$fio, 'email'=>$email, 'birth_date'=>$birth_date, 'gender'=>$gender, 'limbs'=>$limbs, 'superpowers'=>$superpowers, 'biographia'=>$biographia));
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
    print('Данные отправлены');
    exit;
}

?>
